import { createStore } from 'redux';

const rootReducer = (state, action) => {
  return state;
}

export default createStore(rootReducer);